package ApiTest;

import commons.Base;
import org.apache.http.HttpStatus;
import org.junit.Test;

import static io.restassured.RestAssured.*;
import static org.hamcrest.CoreMatchers.*;

public class ApiTest extends Base {

    @Test
    public void testCreateNewUser() {
        String endPoint = "users";
        String data = "{\n" +
                "    \"name\": \"Douglas\",\n" +
                "    \"job\": \"leader\"\n" +
                "}";

        given()
                .contentType("application/json")
                .body(data)
                .when()
                .post(buildUrl(endPoint))
                .then()
                .assertThat()
                .statusCode(HttpStatus.SC_CREATED);
    }

    @Test
    public void testGetSingleUser() {
        String endPoint = "users/2";

        given()
                .when()
                .get(buildUrl(endPoint))
                .then()
                .assertThat().log()
                .all()
                .statusCode(HttpStatus.SC_OK)
                .and()
                .body(
                        "data.id", equalTo(2),
                        "data.email", equalTo("janet.weaver@reqres.in"),
                        "data.first_name", equalTo("Janet"),
                        "data.last_name", equalTo("Weaver"),
                        "data.avatar", equalTo("https://reqres.in/img/faces/2-image.jpg")
                );
    }
}