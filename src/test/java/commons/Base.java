package commons;

public class Base {
    private static final String urlBase = "https://reqres.in/api/";

    public String buildUrl(String endPoint) {
        return urlBase + endPoint;
    }

}
