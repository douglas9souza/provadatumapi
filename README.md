<h1 align="center">Desafio de Automação de testes de API - Datum</h1>

<h2> Sobre o projeto </h2>
<p>Projeto de automação de testes de API, baseado em <b>Maven</b>, com o framework <b>RestAssured</b> utilizando a linguagem de programação <b>Java</b>.</p>
<p>O projeto foi desenvolvido utilizando a IDE <b>IntelliJ</b>.</p>

<p><b>Versão do Java: 1.8.241</b></p>
<p><b>Versão do Maven: 3.6.3</b></p>

<ol><b>Cenários realizados:</b></ol>
<li>Criar um usuário (POST)</li>
<li>Consultar um usuário de determinado ID (GET)</li>

